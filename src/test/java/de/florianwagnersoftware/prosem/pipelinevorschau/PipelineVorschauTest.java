package de.florianwagnersoftware.prosem.pipelinevorschau;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PipelineVorschauTest {

	@Test
	public void testWelcomeMessage() {
		String welcomeMessage = (new PipelineVorschau()).getWelcomeMessage();
		assertEquals("Hallo Test", welcomeMessage);
	}
	
}
